#! /bin/bash

##############################################################################
#
# UIO pruss
##############################################################################

# compiles the LOADER (only) and starts the PRU

#echo "*******************************************************"
#echo "To continue, press any key:"
#read

echo "removing old files"
	rm loaderPRU1
	rm *.bin
 
echo "compiling loader"
	gcc -o loaderPRU1 loader_PRU1.c -lprussdrv                   #Opperate in normal acquisition mode

echo "-Placing the firmware"
	hexpru bin.cmd *.out
#PRU_CORE_USED=$((PRU_CORE+1)) #Dont change this unless you know
#	cp gen/*.out /lib/firmware/am335x-pru$PRU_CORE_USED-fw

echo "-Configuring Pins"
#config-pin -a $HEADER$PIN_NUMBER pruout
	#config-pin -q $HEADER$PIN_NUMBER
	#to see a list of modes use:> config-pin -l P9_41
   
	config-pin -a P8_39 pruout #CS (active low) Chip select
	echo "Chip select:" 
	config-pin -q P8_39
	echo " "
	config-pin -a P8_43 pruout #READ chip clock control
	echo "RD/SCL:" 
	config-pin -q P8_43
	echo " "
	config-pin -a P9_26 pruin #BUSY signal input
	echo "BUSY input" 
	config-pin -q P9_26
	echo " "
	
	config-pin -a P8_46 pruout #RESET_ Pin, not DB4 signal animore
	echo "ADC_RESET_pin as output" 
	config-pin -q P8_46
	echo " "
	config-pin -a P8_40 pruin #DB7 signal input
	echo "DB7 input" 
	config-pin -q P8_40
	echo " "	
	config-pin -a P8_45 pruin #DB8 signal input
	echo "DB8 input" 
	config-pin -q P8_45
	echo " "	
#	config-pin -a P8_21 pruout #DB10 signal input NOT USED IN SERIAL MODE - TO REENABLE WIFI
#	echo "DB10 input" 
#	config-pin -q P8_21
#	echo " "
	config-pin -a P8_29 pruin #S4 button
	echo "S4 button" 
	config-pin -q P8_29
	echo " "
	config-pin -a P8_28 pruout #LED2
	echo "#LED2 debug" 
	config-pin -q P8_28
	echo " "
	config-pin -a P8_27 pruout #LED3
	echo "LED3 debug" 
	config-pin -q P8_27
	echo " "

#config-pin -a P9_92 pruout #pino P9_42B
#echo "ALTERNATIVE MODE TO PIN P9_42B" 
#config-pin -q P9_92
#echo " "
	
#echo "Pulsing RESET pin P8_37" #P8_37 = GPIO2[14] = 2*32 + 14 = 78  
#echo 'out' > /sys/class/gpio/gpio78/direction  #changing to output pin
#echo '1' > /sys/class/gpio/gpio78/value  #changing this pin to high level

#echo "Reseting pin"
#echo '0' > /sys/class/gpio/gpio78/value  #changing this pin to Low level
#echo "Done."
	
	 
#STDBY pin is reset on PRU0deploy

	
	
echo "-Starting PRU"
	chmod 777 ./loaderPRU1
 #nohup creates the file nohup.out (overflow the free memory) - add ">/dev/null 2>&1 &" to dont create nohup.out and execute in background
#nohup 	nice -n -15	 ./loaderPRU1 text.bin data.bin >/dev/null 2>&1 &
	echo "Running with printf Disable"
	nice -n -9	 ./loaderPRU1 text.bin data.bin &    # Run without print
	#echo "Running with printf enable"
	#nice -n -9	 ./loaderPRU1 text.bin data.bin			#use with print
