/*
loader_PRU1.c  -> This files loads the data on PRU1 and interpret the shared memory 

 * Version control:
 *
 * Vr  		/	Date	/ Observation
 *   0.1    / 09-06-18  / Initial version  wi data transfer running ok.
 *  0.2		/ 10-06-18  / Changing the script to store the adc data on the handler FIFO.  
 *  0.3     / 11-06-18  / changing the way to interpret the amount of data to read. (using AlessthanB(A,B)
 *  0.4     / 20-06-18  / Changing to work with automatically reset of read and write pointer by uint16_t overflow. Not based on FS more.
 *  0.5     / 22-06-18  / Implemented a C_INDEPPENDENT_RUN to run without Sampleshandler program. Just PRU1
 *  0.6		/ 09-04-20	/ Added the parameter file support
 *  0.7		/ 10-04-20	/ Adding the PWM freq and compensations delays from parameter file
 *	0.8		/ 12-03-21	/ Adding support for 16 bits data ADCs. AD7606 support.
*/
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include "prussdrv.h"
#include "pruss_intc_mapping.h"
#include <fcntl.h> //Used on FIFO (O_RDONLY, for example)
#include <string.h> //used on FIFO (memset part)
#include <errno.h> //http://www.virtsync.com/c-error-codes-include-errno CODES!!
 
#define PRU_NUM 1			// define which pru is used
#define C_BUFFER_LENGTH 5900 //12 Kb = (6Kwords of 16)-100 initial addrs - FC.  bits must be changed in PRU1 firmware
 
//#define PRU1_DEBUG_COUNTER_FLAG 1  //IF defined, prints the amount data received until the PPS sync
	#define DEBUG_COUNTERSAMPLES 25000 //defines the amount to store and, at the end, print all debug stored data
//#define C_INDEPPENDENT_RUN 1 // Runs alone deactivating the FIFOs. IF defined -> implement the FIFO and comunicate with samplerhandler and bypass FIFO if not defined.
#define C_PARAMETER_FILE "/usr/D-PMU_parameters/D-PMU_parameters.txt"
  
//MEMORY ADDRESSES MAP
#define SHM_OFFSET 0 // 0 is the first position of 12KB shared memory
#define ARM2PROG_PRU1_CONTROL1 0 //memory address to control PRU1 - Start byte control
#define PRU1PROG_WROTE_DATA 7 // This address contains the number of points written by Shared memory
#define ARMPROG_READ_DATA 8 // This address contains the number of points read by the ARM
#define PRU0_PROG_VERSION_ADDR 10// [10] = PRU0 program version
#define PRU1_PROG_VERSION_ADDR 11// [11] = PRU1 program version
#define PROGPRU1_CONTROLDATA1 12// [12 ... 99] = This address identifies what buffer is being written.
#define PROGPRU1_CONTROLDATA2 13// [10 ... 99] = ADC data (address of buffer - buffer count)
#define PROGPRU1_CONTROLDATA3 14// [10 ... 99] = ADC data (??r)
#define PROGPRU1_CONTROLDATA4 15// [10 ... 99] = ADC data (which_buffer) //added by maique
#define PROGPRU1_CONTROLDATA5 16// [10 ... 99] = ADC data (umber of frames 1 iteration) //added by maique
#define PROGPRU1_CONTROLDATA6 17// [10 ... 99] = ADC data (number of frames 2 iteration) //added by maique
#define PROGPRU1_CONTROLDATA7 18// [10 ... 99] = ADC data (number of frames 3 iteration) //added by maique
#define PROGPRU1_CONTROLDATA8 19// [10 ... 99] = ADC data (first k val) //added by maique
#define PROGPRU1_CONTROLDATA9 20// [10 ... 99] = ADC data (last k val) //added by maique
#define LINUX2PRU1_EMULATE_ADDR 40			//Address that identify the operation mode of the PRU1 FW (0 to read data from ADC, 1 to use the emulated data)
#define PRU12LINUX_EMULATE_ADDR 41			//Address that identify the linux of the operation set on the PRU1.
#define LINUX2PRU1_ADC_DATA_LENGTH 43		//Address that identify the adc data length (7609 uses 18 bits, 7606 uses 16 bits)
#define PRU0_TBPRD_VALUE 50					// Address to register that contains the PWM period register value (PWM freq)
#define LINUX2PRU0_NOMINAL_FREQ 56			//Grid nominal frequency


//////////////////////////////////////////////  FIFO DEFINES ///////////////////////////////////
//Path to FIFO - HANDLER2PMU
#define C_FIFO_PATH "/tmp/PRU2samplerFIFO/"
#define C_FIFO_PATHNAME "/tmp/PRU2samplerFIFO/PRU_loadfifo"  /* Path used on ftok for shmget key  */
#define C_FIFO_BUFFER_SIZE 8 //3 cycles //4 values * 32 points //ALready in 16 bits word.
 
//////////////////////////////////////////// MACROS DEFINITIONS ////////////////////////////////
#define ADD_VAL(a,b,r) (r=a+b) // adds two values
#define ADD_ONE(a,r) (r=a+1) //Add one to an value
#define isAlessthanB(a,b) (a<b ? 1 : 0) //Is A less than B
   
///////////////////////////////////////// Global Variables ///////////////////////////////////// 
uint8_t dpmu_emulate_samples_mode;
uint16_t dpmu_pwm_freq_tbprd_reg; //Shared variable for PRU0 freq calculus
uint16_t dpmu_pru1_grid_nominal_freq; //Grid nominal frequency
uint8_t	dpmu_18bits_adc;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 											PRU init function											//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
int pru_init(void)
{
	tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;
	prussdrv_init();
	if (prussdrv_open(PRU_EVTOUT_0))
	{
		return -1;
	}
	else
	{
		prussdrv_pruintc_init(&pruss_intc_initdata);
		return 0;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 											PRU firmware load											//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void pru_load(int pru_num, char* datafile, char* codefile)
{
	// load datafile in PRU memory
	prussdrv_load_datafile(pru_num, datafile);
	// load and execute codefile in PRU
	prussdrv_exec_program(pru_num, codefile);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 											PRU stop function											//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void pru_stop(int pru_num)
{
	prussdrv_pru_disable(pru_num);
	prussdrv_exit();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 										Shared 12k memory config										//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//volatile int32_t* init_prumem()
volatile uint16_t* init_prumem()
{
	//volatile int32_t* p;
	volatile uint16_t* p;
	prussdrv_map_prumem(PRUSS0_SHARED_DATARAM, (void**)&p);
	return p+SHM_OFFSET;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 										PRU magnitudes translate function								//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//Function to convert the read data to a necessary voltage range (Version 1.0) -- Same function aas found on rpmsg_pru31 script
float CALC_AD_VALUE(int16_t AD_read_val){
	float converted_val;
	int range = 10;
	float vref = 2.5;

	converted_val = (float)((AD_read_val*4*2*range)/(131072*(vref/2.5))); //HEY: X4 BECAUSE THAT THE NUMBER IS IN 16 BYTES SIGNED,

	return converted_val;
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 										Functions prototypes											//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void decode_DPMU_file_parameters_pmu(void);

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 												MAIN routine											//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(void)
{
	char step_select = '0'; 
	uint16_t i=0;
	int j=0;
	register uint16_t addr_to_read =(SHM_OFFSET+100); //was volatile
	//uint16_t data_read_count=0;
	register uint16_t read_data_wrote_count =0; //was volatile
	register uint16_t read_data_read_count = 0; //was volatile //memory read registers
	uint32_t RECEIVED_DATA_DEBUG[DEBUG_COUNTERSAMPLES*4]; //debug vector to store de received data
	int received_data_deb_counter =0;
	//Normal mode is emulated samples	
	dpmu_emulate_samples_mode = 0;
	dpmu_18bits_adc = 1;
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Open and mount PRU1 parameters												//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	decode_DPMU_file_parameters_pmu();
	//printf("TBPRD pwm freq register(%d)\n", dpmu_pwm_freq_tbprd_reg);
	//exit(EXIT_SUCCESS);
		
	// initialize the PRU
	printf("pruss driver init (%i)\n", pru_init());
	//stopping old execution on pru
	// load the PRU code (consisting of both code and data bin file).
		// get the memory pointer to the shared data segment
	volatile uint16_t* pruDataMem = init_prumem();
	
	//MEM data initialization
	pruDataMem[ARM2PROG_PRU1_CONTROL1]=0xFFFF;
	pruDataMem[PRU1_PROG_VERSION_ADDR] = 0XFFFF;
	pruDataMem[ARMPROG_READ_DATA] = 0x0000; //None data was read.
	pruDataMem[LINUX2PRU0_NOMINAL_FREQ] = dpmu_pru1_grid_nominal_freq;
	//loading pru
	pru_load(PRU_NUM, "data.bin", "text.bin"); //CAREFUL WITH THE REESPECTIVE NAMES , vary from different setup
	
	//Load and check the information related to the samples emulation mode
	if (dpmu_emulate_samples_mode)
	{
		pruDataMem[LINUX2PRU1_EMULATE_ADDR] = 1;
		puts("Warning: Emulating samples on PRU1");
	}
	else
	{
		pruDataMem[LINUX2PRU1_EMULATE_ADDR] = 0; 
		puts("Warning: Taking data from ADC on PRU1");
	}
	if (dpmu_18bits_adc)
	{
		pruDataMem[LINUX2PRU1_ADC_DATA_LENGTH] = 0xFFFF;
		puts("Warning: Configured 18-bits ADC");
	}
	else
	{
		pruDataMem[LINUX2PRU1_ADC_DATA_LENGTH] = 0x0000;
		puts("Warning: Configured 16-bits ADC");
	}
	
	/// FIFO creation
	//FIFO PIPE - meter2PMU
	int pipe_fd;
	int res;
	int open_mode = O_WRONLY | O_NONBLOCK;//O_RDONLY; //O_NOMBLOCK serve para não bloquear a escrita na fifo e permite debugar quando ela esta cheia por conta da variavel de retorno (=-1 -> fifo full; = 255 -> fifo ok)
	uint16_t buffer_pipe[C_FIFO_BUFFER_SIZE]; //was  [C_FIFO_BUFFER_SIZE + 1]
	
	#ifndef C_INDEPPENDENT_RUN
	puts("oppening fifo...");
	memset(buffer_pipe, '\0', sizeof(buffer_pipe)); // trying define pipo length = 1 full cycle
	//printf("Process %d opening FIFO O_RDONLY\n", getpid());
	pipe_fd = open(C_FIFO_PATHNAME, open_mode);
	if (pipe_fd == -1)
	{
		printf("failed on open FIFO-PRU_loader to Handler");
		fprintf(stderr, "Loader_PRU1 Line 208 fifo open: errno value: %d \n", errno);
		exit(EXIT_FAILURE);
	}
	#endif
	#ifdef C_INDEPPENDENT_RUN
		puts("jumping fifo. Indeppendent mode.");		
	#endif
	   	
	printf("PRU1 ADC interpreter running\n");
	// write to shared data memory - allow pru program
	pruDataMem[ARM2PROG_PRU1_CONTROL1]=0xCAFE; 
	//Just after enable the PRU1, there is performed a check of the decodification of the PRU1 operation mode (ADC)
	if(pruDataMem[PRU12LINUX_EMULATE_ADDR] != 0xEBAA)
	{
		printf("ERROR: PRU1 doesnt check the operation mode (adc read or emulated)"); 
		printf("Value decoded = %X\n", pruDataMem[PRU12LINUX_EMULATE_ADDR+1]);
		printf("Value answer = %X\n", pruDataMem[PRU12LINUX_EMULATE_ADDR]);
	}
	
	while(1)
	{	 

		#ifdef PRU1_DEBUG_COUNTER_FLAG //PRU1_DEBUG_COUNTER_FLAG == 1){
		if ( received_data_deb_counter >= (DEBUG_COUNTERSAMPLES*4))
		{
			printf("Last iteration amount is %d\n",pruDataMem[PROGPRU1_CONTROLDATA9]);
			printf("Loosed points are %d\n",pruDataMem[PROGPRU1_CONTROLDATA8]);
			printf("FS is  %d\n",pruDataMem[PROGPRU1_CONTROLDATA7]);
			printf("debug value %d\n",pruDataMem[PROGPRU1_CONTROLDATA6]);
			for (j=0; j< (DEBUG_COUNTERSAMPLES*4);j=j+4)
			{
				printf("counter is %d, Va = %f, Vb = %f, Vc = %f   \\    original Va = %d , Vb = %d , Vc = %d \n", RECEIVED_DATA_DEBUG[j],CALC_AD_VALUE(RECEIVED_DATA_DEBUG[j+1]),CALC_AD_VALUE(RECEIVED_DATA_DEBUG[j+2]),CALC_AD_VALUE(RECEIVED_DATA_DEBUG[j+3]),RECEIVED_DATA_DEBUG[j+1],RECEIVED_DATA_DEBUG[j+2],RECEIVED_DATA_DEBUG[j+3]);
			}
			
			//pru_stop(PRU_NUM);
			exit(EXIT_SUCCESS);
		}

		#endif 
	 
		read_data_wrote_count = pruDataMem[PRU1PROG_WROTE_DATA];
		read_data_read_count  = pruDataMem[ARMPROG_READ_DATA];

		if( isAlessthanB(read_data_read_count , read_data_wrote_count) )
		{
		
			buffer_pipe[0] = pruDataMem[addr_to_read];
			buffer_pipe[1] = pruDataMem[addr_to_read+1];
			buffer_pipe[2] = pruDataMem[addr_to_read+2];
			buffer_pipe[3] = pruDataMem[addr_to_read+3];
			#ifndef C_INDEPPENDENT_RUN //C_INDEPPENDENT_RUN == 0){
			res = write(pipe_fd, buffer_pipe, C_FIFO_BUFFER_SIZE); //4 valuse of 16 bits = 8 bytes
			if (res == -1) //try write until fifo is not full error handler
			{
				 //printf("sent the value  %02X %02X // %02X %02X // %02X %02X //120e121 = %02X %02X \n //", buffer_pipe[0],buffer_pipe[1],buffer_pipe[8], buffer_pipe[9], buffer_pipe[16], buffer_pipe[17], buffer_pipe[120], buffer_pipe[121]);
				while(res < 0)
				{
					fprintf(stderr, "Loader_PRU line 265: trying_to_write_fifo: errno value: %d \n", errno);
					puts("PRU1 Loader: FIFO Loader 2 PMU_handler escrita com erro (Read<write) - Nova tentativa para escrever na fifo");
					res = write(pipe_fd, buffer_pipe, C_FIFO_BUFFER_SIZE);
					if (res == -1) //Scnd try failed.
					{
						exit(EXIT_FAILURE);
					}
				}
			 }
			#endif
			//DEBUG store
			#ifdef PRU1_DEBUG_COUNTER_FLAG
				RECEIVED_DATA_DEBUG[received_data_deb_counter] = buffer_pipe[0];
				RECEIVED_DATA_DEBUG[received_data_deb_counter+1] = buffer_pipe[1];
				RECEIVED_DATA_DEBUG[received_data_deb_counter+2] = buffer_pipe[2];
				RECEIVED_DATA_DEBUG[received_data_deb_counter+3] = buffer_pipe[3];
				ADD_VAL(received_data_deb_counter,4,received_data_deb_counter); 
			#endif
			//end debug store
			//FIFO end write
			pruDataMem[ARMPROG_READ_DATA] =  ADD_ONE(read_data_read_count,read_data_read_count); //increment of sample read
			if (addr_to_read == (C_BUFFER_LENGTH+100-4)) //+100 because the initial addr, and -4 because that it alread dont sum the 4 readed bytes on this var
			{
				addr_to_read = (SHM_OFFSET+100);
			}
			else
			{
				ADD_VAL(addr_to_read,4,addr_to_read);
				//addr_to_read = addr_to_read + 4;
			}
		
		}/* //isAlessthanB(read_data_read_count,read_data_wrote_count)*/
		else if( isAlessthanB(read_data_wrote_count, read_data_read_count) )
		{
			//printf("Olha a MMMM... mudou aqui para val wrote = %d e val read = %d \n ",read_data_wrote_count, read_data_read_count);
				//printf("reading data. \n");
			buffer_pipe[0] = pruDataMem[addr_to_read];
			buffer_pipe[1] = pruDataMem[addr_to_read+1];
			buffer_pipe[2] = pruDataMem[addr_to_read+2];
			buffer_pipe[3] = pruDataMem[addr_to_read+3];
	//IMPORTANT DEBUG
	//			printf("2 addr is %d, counter is %d\n",addr_to_read, pruDataMem[addr_to_read]);
	//			printf("VALUE C is %d\n",pruDataMem[addr_to_read+3]);
			#ifndef C_INDEPPENDENT_RUN
			//FIFO end write
			res = write(pipe_fd, buffer_pipe, C_FIFO_BUFFER_SIZE); //4 valuse of 16 bits = 8 bytes
			if (res == -1) //try write until fifo is not full error handler
			{
				 //printf("sent the value  %02X %02X // %02X %02X // %02X %02X //120e121 = %02X %02X \n //", buffer_pipe[0],buffer_pipe[1],buffer_pipe[8], buffer_pipe[9], buffer_pipe[16], buffer_pipe[17], buffer_pipe[120], buffer_pipe[121]);
				while(res < 0)
				{
				 fprintf(stderr, "Loader_PRU line 313: trying_to_write_fifo: errno value: %d \n", errno);
				 puts("PRU1 Loader: FIFO Loader 2 PMU_handler Cheia - Aguardando leitura da PMU para escrever na fifo");
				 res = write(pipe_fd, buffer_pipe, C_FIFO_BUFFER_SIZE);
				 if (res == -1) //Scnd try failed.
					{
						exit(EXIT_FAILURE);
					}
				}
			}	 
			#endif
				
			//DEBUG store
			#ifdef PRU1_DEBUG_COUNTER_FLAG
				RECEIVED_DATA_DEBUG[received_data_deb_counter] = buffer_pipe[0];
				RECEIVED_DATA_DEBUG[received_data_deb_counter+1] = buffer_pipe[1];
				RECEIVED_DATA_DEBUG[received_data_deb_counter+2] = buffer_pipe[2];
				RECEIVED_DATA_DEBUG[received_data_deb_counter+3] = buffer_pipe[3];
				ADD_VAL(received_data_deb_counter,4,received_data_deb_counter); 
			#endif
			//end debug store
				
			ADD_ONE(read_data_read_count,read_data_read_count);
			if (read_data_read_count == 0)
			{
				pruDataMem[ARMPROG_READ_DATA] =  ADD_ONE(read_data_read_count,read_data_read_count); //increment of sample read
			}
			else
			{
				pruDataMem[ARMPROG_READ_DATA] = read_data_read_count;
			}
			//read_data_read_count++;
			if (addr_to_read == (C_BUFFER_LENGTH+100-4)) //+100 because the initial addr, and -4 because that it alread dont sum the 4 readed bytes on this var
			{
				addr_to_read = (SHM_OFFSET+100);
			}
			else
			{
				ADD_VAL(addr_to_read,4,addr_to_read);
				//addr_to_read = addr_to_read + 4;
			}
			
		} //end if isAlessthanB

	} //while1
	
}
	
	
	
/***************************************************************************************************
* Function to read and decode PRU1 parameters from PARAMETER file.
*
*
***************************************************************************************************/	
void decode_DPMU_file_parameters_pmu(void)
{

	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	size_t length, length_header;

	//Open file
	fp = fopen(C_PARAMETER_FILE, "r");
	//check file health
	if (fp == NULL)
	{
		printf("Error on PRU1 deploy. The Parameters file is empty.");
		exit(EXIT_FAILURE);
	}
	
	puts("--------------------Decoding process starts--------------------------------");

	//structure reset.
	//memset(&interpreted_data, 0x00, sizeof(interpreted_data));

	//Read and decode important lines (with '*' identifier )
	while ((length = getline(&line, &len, fp)) != -1) //seek file until it ends
	{
		if(line[0] == '*') //parameter identifier
		{
			if(strncmp(line,"*D-PMU_EMULATE_MODE",19) == 0)
			{
				length = getline(&line, &len, fp); //take response from ask to send to other PDC
				printf("PRUSS emulate mode? %s\r\n", line);
				if(strncmp(line,"yes",3) == 0)
				{
					dpmu_emulate_samples_mode = 1;
				}
			}
			else if(strncmp(line,"*ADC_DATA_LENGTH",15) == 0)
			{
				length = getline(&line, &len, fp); //take response from ask to send to other PDC
				printf("D-PMU ADC length: %s\r\n", line);
				if(strncmp(line,"18bit",5) == 0)
				{
					dpmu_18bits_adc = 1;
				}
				else
				{
					dpmu_18bits_adc = 0;
				}
			}
			else if(strncmp(line,"*PWM_FREQ_TBPRD_EPWM",20) == 0)
			{
				length = getline(&line, &len, fp); //of the PWM period register.
				printf("pwm period: %s\r\n", line);
				dpmu_pwm_freq_tbprd_reg = atoi(line);
			}
			else if (strncmp(line,"*NOMINAL_SYS_FREQUENCY",22) == 0)
			{
				length = getline(&line, &len, fp);
				dpmu_pru1_grid_nominal_freq = atoi(line);
				printf("Nominal configured frequency: %d\r\n", dpmu_pru1_grid_nominal_freq);
			}
			
			else
			{

			}
		}

	}
	puts("--------------------Decoding process finished--------------------------------");

	fclose(fp);
	//if (line)
	//	free(line);
	//exit(EXIT_SUCCESS);

}
