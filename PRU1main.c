/*
 * mainPRU1.c (changed to PRU1main.c) -- drive UIO
 *
 *
 *
 * Version control:
 *
 * Vr  		/	Date	/ Observation
 * 0.1		/ 24-05-18	/ Initial version
 * 0.2		/ 04-06-18  / change to send just bytes MSB and LSB, not bits with char 0 a 1 to rpmsg pipe.
 * 0.3		/ 06-06-18  / change RPMSG to send 2 sampled points by rpmsg send op. 1 cycle is too much in case where get late access. 1 point causes data loss. So i adopt 2 points per data communcation
 * 0.4		/ 07-06-18  / Well, N�O CONSEGUI SHARED MEMORY.. VOU TENTAR REGISTRAR O RPMSG e enviar o buffer full
 * 0.5      / 08-06-18  / Esquece.. Estou pelo DRIVE UIO. Aqui tenho o compartilhamento de mem�ria.
 * 0.6		/ 20-06-18	/ ADD the count reset using the PWMSS sync register.
 * 0.7		/ 20-06-18  / Make the treatment to get only FS samples, If more, freeze the memory share counter. If less, must jump to next addr many samples as necessary to reach the FS points.
 * 0.8      / 22-06-18  / Implemented an serial data read communication based on SPI protocol
 * 0.9		/05-07-18	/ Changed ADC DB4 pin to RESET pin
 * 1.0 		/06-07-18	/ Adjusting the reading time.
 * 1.1		/07-08-18	/ Fix the unread first sample because the redundant while (busy==1) logic.The 2 requisition are too near that after the first wait busy, the waveforme goes down and was set again (in this time, the 2cnd wait busy did detect and lost first sample).
 * 1.2		/ 28-11-18  / Addecquate to D-PMU cape v2
 * 1.3		/ 30-12-18  / Changed to improved efficiency and cycle save operations
 * 1.4		/ 14-07-19	/ Insercao de definicoes para modo simulado onde o PWM � setado pela PRU0 e a PRU1 verifica o contador dele, semrpe que o contador chegar na contagem de FS, a PRU1 vai enviar uma amostra. Neste contexto, os pinos de requisi��o vao operar, mas o controle pelo busy ser�o desativados.
 * 1.5		/ 25-11-20	/ Support for 50Hz nominal frequency systems
 * 1.6		/ 12-03-21	/ Adding support for 16 bits data ADCs. AD7606 support.
 *
 * //NOTES:
//	Interrupts on __R31:
//	35 = PRU_EVTOUT0
//  36 = PRU_EVTOUT1 and ... until PRU_EVTOUT7
// INPUT COMPARISSON:
//	 __R31&&(1<<14) == (1<<14) this is correct
 ///////////
//
 *
 //TODO: REMOVER A VARIAVEL freeze_addrs_flag pois esta estava sendo usada para, quando houvesse perda de GPS, a PMU parasse de LER as amostras.
 //TODO: Deixar por default o reset do contador quando chegar e ultrapassar os NUMEROSDEPONTOSPORSEGUNDO.
 *
 *

 * PINs and R30 and R31 positions  FOR PRU1 (ONE)
 *  PortPin /  __R30 (1<<X) /   mapped to   /     __R31 (1<<X)/ mapped to
 * P8_20	/		13		/    			/	   	13		/
 * P8_21	/		12		/    			/		12		/
 * P8_27	/		8		/     LED3 		/		8		/
 * P8_28	/		10		/     LED2		/		10		/
 * P8_29	/		9		/    			/		9		/
 * P8_30	/		11		/    			/		11		/
 * P8_39	/		6		/ CS(ativo low)	/		6		/
 * P8_40	/		7		/	 	 		/		7		/	DB7 //dont touched (also used for serial)
 * P8_41	/		4		/	 			/		4		/
 * P8_42	/		5		/	 			/		5		/
 * P8_43	/		2		/RD/SCL(ativo low)/		2		/
 * P8_44	/		3		/	 			/		3		/	DB8 (for capes higher than Version 3, included)
 * P8_45	/		0		/	 			/		0		/	DB8 (for capes less than Version 3)
 * P8_46	/		1		/	RESET		/		1		/
 * P9_26	/	none		/	 			/		16		/  	BUSY
*/
/************************************************************
* STANDARD BITS
************************************************************/
//16LSB
#define BIT0                   (0x00000001)
#define BIT1                   (0x00000002)
#define BIT2                   (0x00000004)
#define BIT3                   (0x00000008)
#define BIT4                   (0x00000010)
#define BIT5                   (0x00000020)
#define BIT6                   (0x00000040)
#define BIT7                   (0x00000080)
#define BIT8                   (0x00000100)
#define BIT9                   (0x00000200)
#define BIT10                  (0x00000400)
#define BIT11                  (0x00000800)
#define BIT12                  (0x00001000)
#define BIT13                  (0x00002000)
#define BIT14                  (0x00004000)
#define BIT15                  (0x00008000)
//16MSB
#define BIT16                  (0x00010000)
#define BIT17                  (0x00020000)
#define BIT18                  (0x00040000)
#define BIT19                  (0x00080000)
#define BIT20                  (0x00100000)
#define BIT21                  (0x00200000)
#define BIT22                  (0x00400000)
#define BIT23                  (0x00800000)
#define BIT24                  (0x01000000)
#define BIT25                  (0x02000000)
#define BIT26                  (0x04000000)
#define BIT27                  (0x08000000)
#define BIT28                  (0x10000000)
#define BIT29                  (0x20000000)
#define BIT30                  (0x40000000)
#define BIT31                  (0x80000000)

/////////////////////////////////////////////////////////////////////
// INCLUDES
#include "stdint.h"
//#include <am335x.h> // https://github.com/ehayon/BeagleBone-GPIO/blob/master/src/am335x.h --have defines to pins configuration
#include "registers.h"
#include <stdio.h>
#include <pru_cfg.h>
#include <sys_pwmss.h> //used to get access to PWM sync in register

///////////////////////////////////DEFINES FOR THIS PROGRAM//////////////////////////////////////////////
#define C_NORMAL_OPPERATION			//Output Leds work as PRU on and 1Hz indicators.
#ifndef C_NORMAL_OPPERATION
	#define C_DEBUG_OPPERATION		//Output Leds work as debug leds
#endif
#define C_SAMPLED_FREQUENCY_SIXTY 3840//15360//15360//7680//3840//1920 // sames as points per second must be changed in loader too
#define C_SAMPLED_FREQUENCY_FIFTY 3200 //Must check this parameter in loader too

#define C_SAMPLES_PER_CYCLE 64
#define C_WORK_WITHOUT_GPS 0 // 1 to work without GPS, 0 to work with PPS
#define C_PROG_VERSION 3 //PRU program version. kind of debug on linux side


#define C_OUTPUT_RESET BIT1 //P8_46 RESET signal
#define C_CS_OUTPUT BIT6 		// P8_39 Chip select OUTPUT
#define C_RDSCL_OUTPUT BIT2 	// P8_43 chip data read clock
#define C_INPUT_BUTTON_S4 BIT9//P8_29 same as P9_91 for input
#define C_OUTPUT_LED2_PRU1 BIT10 	//P8_27 for output LED2
#define C_OUTPUT_LED3_1PPS BIT8 	//P8_28 for output LED3
#define C_BUSY_INPUT BIT16 // P9_26 input data bit

#define C_DBoutA_SPI_RD BIT7 // same as DB7 data. pin P8_40
#define C_DBoutB_SPI_RD BIT0 // same as DB8 pin. P8_45


//#define C_DELAY_RDSCL_HPULSE 100 //5 is to low with this board 7 = 35 ns 50 ns = 62.5ns na pr�tica
#define C_DELAY_RDSCL_LPULSE_BEFORE_READ 25 //5 is to low with this board 7 = 35 ns 50 ns = 62.5ns na pr�tica THIS VALUE ISNT POSSIBLE TO BE SEE ON ANALOGIC ANALYZER (16MHZ)
//#define C_DELAY_RDSCL_TRASH_LPULSE 100 //5 is to low with this board 7 = 35 ns 50 ns = 62.5ns na pr�tica
//#define C_CONVERSIONS_AMOUNT_LIMIT 2 //max conversions
#define C_AMOUNT_BYTES_PER_SENT_OPP 2 //send two sampled points per ARM communication action

//SEEE IF THESES VALUES ARE CORRECTLY
#define C_BUFFER_LENGTH 5900 //6000 //must be changed on the loader tooo -CAREFULL



//////////////////////////////////MACROS to save cycles ////////////////////////////////////////////////
#define COMPARE_VAL(a,b) (a==b ? 1 : 0) // compares 2 values
#define ADD_VAL(a,b,r) (r=a+b) // adds two values
#define ADD_ONE(a,r) (r=a+1) //Add one to an value
#define SET_ZERO(r) (r=0)
#define SET_ONE(r) (r=1)
#define min(a,b) (a<b ? a : b)
//----------------------------------------------------------------------------------------
#define SET_BITV2(p,n) (p |= n) //set the bit N
#define CLR_BITV2(p,n) (p &= ~n) // clear the bit N
#define TGL_BITV2(p,n) (p ^= n)  //Toggle the specified bit n



/////////////////////////////////FUNCTIONS/////////////////////////////////////////////////////////////
void REQUEST_CONVERSION();
//////////////////////////////////////////////////////////////////////////////////////////////////////
//GPIO parameters
volatile register uint32_t __R31, __R30;  //R31 =  INPUT; R30 = Output
//uint32_t sampled_data_V1,sampled_data_V2,sampled_data_V3;//,sampled_data_V4,sampled_data_V5,sampled_data_V6,sampled_data_V7,sampled_data_V8; --parallel MODE
uint16_t sampled_data_V1,sampled_data_V2,sampled_data_V3;//,sampled_data_V4,sampled_data_V5,sampled_data_V6,sampled_data_V7,sampled_data_V8; // Serial mode
//uint16_t adc_dataV1[4] ;

///////////////////////////  PWM ACCESS REGISTER DEFINITION /////////////////////////////////////////
/* Non-CT register defines */
#define CM_PER_EPWMSS0 (*((volatile unsigned int *)0x44E000CC))

volatile sysPwmss PWMSS0 __attribute__((cregister("PWMSS0", near), peripheral));



/* Mapping Constant table register to variable */
volatile pruCfg CT_CFG __attribute__((cregister("CFG", near), peripheral));



//Debug inertial phases
//64 points per cycle
int AD_read_val_faseA[64] = {1285, 2557, 3805, 5016, 6179, 7282, 8315, 9268, 10132, 10898, 11560, 12109, 12543, 12855, 13044, 13107,
							13044, 12855, 12543, 12109, 11560, 10898, 10132, 9268, 8315, 7282, 6179, 5016, 3805, 2557, 1285, 0,
							-1285, -2557, -3805, -5016, -6179, -7282, -8315, -9268, -10132, -10898, -11560, -12109, -12543, -12855, -13044, -13107,
							-13044, -12855, -12543, -12109, -11560, -10898, -10132, -9268, -8315, -7282, -6179, -5016, -3805, -2557, -1285, 0
							};

 int AD_read_val_faseB[64] = {-11939, -12412, -12765, -12995, -13100, -13079, -12932, -12661, -12267, -11755, -11131, -10399, -9566, -8642, -7635, -6554,
							-5409, -4213, -2976, -1711, -429, 857, 2135, 3392, 4617, 5797, 6921, 7979, 8960, 9855, 10654, 11351,
							11939, 12412, 12765, 12995, 13100, 13079, 12932, 12661, 12267, 11755, 11131, 10399, 9566, 8642, 7635, 6554,
							5409, 4213, 2976, 1711, 429, -857, -2135, -3392, -4617, -5797, -6921, -7979, -8960, -9855, -10654, -11351
							};

 int AD_read_val_faseC[64] = {10654, 9855, 8960, 7979, 6921, 5797, 4617, 3392, 2135, 857, -429, -1711, -2976, -4213, -5409, -6554,
							-7635, -8642, -9566, -10399, -11131, -11755, -12267, -12661, -12932, -13079, -13100, -12995, -12765, -12412, -11939, -11351,
							-10654, -9855, -8960, -7979, -6921, -5797, -4617, -3392, -2135, -857, 429, 1711, 2976, 4213, 5409, 6554,
							7635, 8642, 9566, 10399, 11131, 11755, 12267, 12661, 12932, 13079, 13100, 12995, 12765, 12412, 11939, 11351
							};

/////////////////////////////////////////////////////////////////////
// GLOBALS
//

/////////////////////////////////////////////////////////////////////
// MEMORY MAP SCHEME:
#define ARM2PROG_PRU1_CONTROL1 0// [0] = ARM program control 1 PRU1 //changed
#define ARM2PROG_CONTROL2 1// [1] = ARM program control 2 PRU0
#define PROG2ARM_CONTROL1 2// [2] = ARM send info address 1 PRU0
#define PROG2ARM_CONTROL2 3// [3] = ARM send info address 2 PRU0
// [4..9] = Reserved PRU1

#define PRU1PROG_STATUS_ADDR 6 // Status control to arm program know where the PRU1 program is.
#define PRU1PROG_WROTE_DATA 7 // This address contains the number of points written by Shared memory
#define ARMPROG_READ_DATA 8 // This address contains the number of points read by the ARM
//#define PRU1PROG_ADDRESS_TO_READ 9 // This address contain the number of addres to ARM perform an read opperation


#define PRU0_PROG_VERSION_ADDR 10// [10] = PRU0 program version //changed
#define PRU1_PROG_VERSION_ADDR 11// [11] = PRU1 program version
#define PROGPRU1_CONTROLDATA1 12// [12 ... 99] = This address identifies what buffer is being written.


#define PROGPRU1_CONTROLDATA2 13// [10 ... 99] = ADC data (address of buffer - buffer count)
#define PROGPRU1_CONTROLDATA3 14// [10 ... 99] = ADC data (??r)
#define PROGPRU1_CONTROLDATA4 15// [10 ... 99] = ADC data (which_buffer) //added by maique

#define PROGPRU1_CONTROLDATA5 16// [10 ... 99] = ADC data (umber of frames 1 iteration) //added by maique
#define PROGPRU1_CONTROLDATA6 17// [10 ... 99] = ADC data (number of frames 2 iteration) //added by maique
#define PROGPRU1_CONTROLDATA7 18// [10 ... 99] = ADC data (number of frames 3 iteration) //added by maique

#define PROGPRU1_CONTROLDATA8 19// [10 ... 99] = ADC data (first k val) //added by maique
#define PROGPRU1_CONTROLDATA9 20// [10 ... 99] = ADC data (last k val) //added by maique


//#define PROGPRU0_CONTROLDATA5 16// [10 ... 99] = ADC data () //added by maique
#define PROGPRU0_INITIALDATA_ADDR 100 // PRU0 initial data valid addr
///ADDr from 101 to 60100 are reseved to Shared memory ( 12Kb)

#define LINUX2PRU1_EMULATE_ADDR 40			//Address that identify the operation mode of the PRU1 FW (0 to read data from ADC, 1 to use the emulated data)
#define PRU12LINUX_EMULATE_ADDR 41			//Address that identify the linux of the operation set on the PRU1.
#define LINUX2PRU1_ADC_DATA_LENGTH 43		//Address that identify the adc data length (7609 uses 18 bits, 7606 uses 16 bits)
#define PRU1_TO_PRU0_SYNC_CNTR 55			//Counter that is incremented always an 1 PPS sync ocurs
#define LINUX2PRU0_NOMINAL_FREQ 56			//Grid nominal frequency


//GPIO parameters
volatile register unsigned int __R31, __R30;


volatile uint16_t* shared_ram;
volatile uint16_t* buffer;
//volatile uint16_t* buffer2;
volatile uint16_t sample_counter = 1; //This is incremented by one
volatile uint16_t write_pointer_count = 0;  //careful with this data type. It should be the same as defined on loader to overflow toguether
volatile uint16_t samples_per_second = 3840; //default value for 60 Hz system
volatile uint8_t adc_18bit_datalength; //default is AD7609 (18bit)


//#define C_DESIRED_SAMPLES 15
#define C_CONTROL_RESERVED_ADD 100


int main(int argc, const char *argv[]){


	uint16_t read_adc_or_emulate_operation_mode = 0;						//Default of the mode is read adc operation mode
	uint8_t	emulated_mode_sample_generated = 0;
	uint16_t sinc_1pps_counter;

	//Removing StandBy to SYSCFG PYPE
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

	// Init globals
	shared_ram = (volatile uint16_t *)0x10000;  //memory shared (ARM-PRUs/PRUs-PRUs)

	buffer = &(shared_ram[100]); // We'll start putting samples in shared ram
								// address 100 and use up to 6KB = 4 bytes
	//buffer2 = &(shared_ram[3000]); // We'll start putting samples in shared ram
								// address 100 and use up to 6KB = 4 bytes
	//Shared memory reset values:
	shared_ram[PRU1PROG_STATUS_ADDR] = 0xEEEE; //initial value to status.
	shared_ram[PRU1PROG_WROTE_DATA] = 0x0000; //none data wrotten


	// Locals:
	uint8_t freeze_addrs_flag =0;
	uint16_t loosed_points = 0;
	unsigned int i;
//	uint16_t buffer_count = 0;
	volatile uint16_t  memADDR = 0;

	//Reset dos LEDS de debug.
	CLR_BITV2(__R30,C_OUTPUT_LED2_PRU1); 	//RESET LED2
	CLR_BITV2(__R30,C_OUTPUT_LED3_1PPS); 	//RESET LED3


	//reseting adc board
	CLR_BITV2(__R30,C_OUTPUT_RESET); 	//RESET normal state
	__delay_cycles(25);
	SET_BITV2(__R30,C_OUTPUT_RESET); 	//RESET
	__delay_cycles(25);
	CLR_BITV2(__R30,C_OUTPUT_RESET); 	//RESET normal state


//	uint8_t adc_finaldataV2[50], adc_finaldataV3[50]; //12 bits.. 16 bits are sufficient
//	uint16_t communication_counter = 0; //this is incremented by 8 (8 bytes per point)
//	uint8_t send_flag = 0; // 0=dont send data, 1 = send data




	//INITIAL SETTINGS
	//SET_BIT(__R30,C_CS_OUTPUT); 	//iddle state to chip select
	SET_BITV2(__R30,C_CS_OUTPUT); 	//iddle state to chip select
	SET_BITV2(__R30,C_RDSCL_OUTPUT); 	//iddle stagte to read clock
	shared_ram[PRU1PROG_WROTE_DATA] = write_pointer_count;//sample_counter; //none data wrotten //automatically reset when 2^16 get reached





	while(shared_ram[ARM2PROG_PRU1_CONTROL1] != 0xCAFE)
	{ //BABE
		//take operation mode from the Linux program loader program
		read_adc_or_emulate_operation_mode = shared_ram[LINUX2PRU1_EMULATE_ADDR];			//Take the operation mode configuration from C linux

		//Take the ADC data length
		if(shared_ram[LINUX2PRU1_ADC_DATA_LENGTH] != 0x0000) //LINUX2PRU1_ADC_DATA_LENGTH
		{
			adc_18bit_datalength = 1; //18 bits adc
		}
		else
		{
			adc_18bit_datalength = 0; //16 bits adc
		}

		//Check if the operation mode is decoded successful
		if( (read_adc_or_emulate_operation_mode == 0x0001) || (read_adc_or_emulate_operation_mode == 0x0000) )
		{
			shared_ram[PRU12LINUX_EMULATE_ADDR] = 0xEBAA; 									//Return the operation mode to C linux program
			shared_ram[PRU12LINUX_EMULATE_ADDR+1] = read_adc_or_emulate_operation_mode;
		}
		else
		{
			shared_ram[PRU12LINUX_EMULATE_ADDR] = 0xAFFF; 									//Return the operation mode to C linux program
			shared_ram[PRU12LINUX_EMULATE_ADDR+1] = read_adc_or_emulate_operation_mode;
		}
		//take nominal frequency
		if(shared_ram[LINUX2PRU0_NOMINAL_FREQ] == 60)
		{
			samples_per_second = C_SAMPLED_FREQUENCY_SIXTY;
		}
		else
		{
			samples_per_second = C_SAMPLED_FREQUENCY_FIFTY;
		}
	} //Wait the start word from ARM - as an kick.




//debug write defined values on memory. MEMORY = 16 bits;
	for (i=0; i <= 6000;i++)
	{
		//shared_ram[i]=i;
		//shared_ram[i]=0xAAAA;
		//buffer[i]=0xBBBB;
		buffer[i]=0xCCCC;
	}



   shared_ram[PRU1_PROG_VERSION_ADDR] = C_PROG_VERSION;

   SET_BITV2(PWMSS0.EPWM_TBSTS,BIT0); //bit 1 is ///reset to get in while
   if(C_WORK_WITHOUT_GPS == 0)
   {
	   //while(!((PWMSS0.EPWM_TBSTS & 0x0002)==0x0002)){ //verify if an the bit SYNCI of TBSTS is RESET
	   while( (PWMSS0.EPWM_TBSTS&BIT1) == 0 )
	   { //verify if an the bit SYNCI of TBSTS is RESET
			CLR_BITV2(__R30,C_RDSCL_OUTPUT); //enabling read
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ);
			SET_BITV2(__R30,C_RDSCL_OUTPUT);
		}
   }

   PWMSS0.EPWM_TBSTS |= BIT6;  	//Force a synchronization event

   sinc_1pps_counter = 0;
   shared_ram[PRU1_TO_PRU0_SYNC_CNTR] = 0x0000;	//No synchronization is contabilized
   shared_ram[PRU1PROG_STATUS_ADDR] = 0x0000; //will start the conversion
   buffer[0] = 0; //first counter
   i = 0;
   SET_BITV2(__R30,C_OUTPUT_LED2_PRU1);
   while(1)
   {

	   ///////////////////////  Verify if the operation mode gets emulated samples  (withou ADC) or real samples (with ADC) //////////////////
	   if(read_adc_or_emulate_operation_mode == 0x0000)
	   { 				//Reads from ADC module

		if( __R31&C_BUSY_INPUT )
		{ // Busy signal goes high, an conversion is occurring
			//verify if this is the first point cycle (based on the SYNCI (GPS) input to PWM)
			loosed_points = 0;

			while ( __R31&C_BUSY_INPUT )
			{
			}

//			__delay_cycles(10); //Give to busy a chance to detec the Sync puse before to exclude this sample


			if(PWMSS0.EPWM_TBSTS & BIT1)
			{ //verify if an the bit SYNCI of TBSTS was set

				//Syncs PRU0
				sinc_1pps_counter++;
				shared_ram[PRU1_TO_PRU0_SYNC_CNTR] = sinc_1pps_counter;	//Syncs a PRU0

				///VERIFICA ISSO.. TEM QUE SETAR 2 MAS J� EST� FENTO ISTO NA PRU0
				//SET_BIT(PWMSS0.EPWM_TBSTS,2); //1 is to reset the SYNC register
				SET_BITV2(PWMSS0.EPWM_TBSTS,BIT1); //1 is to reset the SYNC register
				//PWMSS0.EPWM_TBSTS = 0x0002;

				//jumping the address if the PWM requested less points as needed. Ex (1910points - 1920 Hz)
				if((sample_counter < samples_per_second ) & (sample_counter > 1) )
				{
					loosed_points = (samples_per_second - sample_counter + 1 ); //+1 could be a problem, i put it to match the vectors
					//maybe must write specific values to the loosed points;
					for (i = 1; i <=loosed_points; i++)
					{ //increment as much as necessary the pointers values
						if(write_pointer_count == 0xFFFF)
						{
							write_pointer_count = 1;
						}
						else
						{
							write_pointer_count++; //increment the write counter to identify a new data
						}
						if (COMPARE_VAL(memADDR,(C_BUFFER_LENGTH-4)))
						{
							memADDR = 0;
						}
						else
						{
							ADD_VAL(memADDR,4,memADDR); //each point has 4 words with 16 bits length
						}
					} //for i
				} // sample_counter < C_SAMPLED_FREQUENCY
				//debug address
				//theses values are set after an PPS rise
				shared_ram[PROGPRU1_CONTROLDATA9] = sample_counter-1;// + sample_points;
				shared_ram[PROGPRU1_CONTROLDATA8] = loosed_points;// + loosed_points;
				shared_ram[PROGPRU1_CONTROLDATA7] = samples_per_second;// + loosed_points;
				shared_ram[PROGPRU1_CONTROLDATA6] = 111;// + loosed_points;

				//reseting counter
				sample_counter = 1;
				//sample_counter = 1;

				freeze_addrs_flag = 0;

				//PWMSS0.EPWM_TBSTS=0x0002; //clear latched event (must write 1 in the SYNCI position - THE OTHER BITS VALUES HAVE NO EFFECTS, so is possible to write zero in all other bits
			}


			if( sample_counter <= samples_per_second)
			{
				// WRITER POINT TREATMENT with address jump or shift back based on TBSTS or number of samples_per_second_reached
				if(write_pointer_count == 0xFFFF)
				{
					write_pointer_count = 1;
				}
				else
				{
					write_pointer_count++; //increment the write counter to identify a new data
				}
				REQUEST_CONVERSION(); //reativar isso
				//CLR_BIT(__R30,C_CS_OUTPUT); //enabling data transfer
			}
			else
			{
//sample_counter = 0; //TESTE PARA CONTINUAR FUNCIONANDO COM PERDA DE GPS. Descomente para resetar
				//while( COMPARE_VAL((__R31&(1<<C_BUSY_INPUT)) , (1<<C_BUSY_INPUT))){}; //wait until busy goes low dow.
freeze_addrs_flag = 1; // ex: 1923 points per 1920 Hz
				if(C_WORK_WITHOUT_GPS == 1)
				{
					freeze_addrs_flag =0;
					REQUEST_CONVERSION(); //reativar isso
					if(write_pointer_count == 0xFFFF)
					{
						write_pointer_count = 1;
					}
					else
					{
						write_pointer_count++; //increment the write counter to identify a new data
					}
				}
			}


			//COMMUNICATION COUNTER
			//TODO: Tentar colocar colocar aqui o reset do sample_counter (mas precisa revisar estas logicas extras acimas...)
			//ESTES BITS, MESMO SENDO 18 BITS, S�O MONTADOS E FECHADOS EM PACOTES DE 16 BITS DE PALAVRA (12 bits validos + bit de sinal 4 vezes). No lado do ARM que � feito o deslocamento restante para MSB dos 18 bits (preservando o sinal)
			buffer[memADDR] = sample_counter; //1 until FS

			//V1
			//PARALLEL -> buffer[memADDR+1] = ((sampled_data_V1&(1<<C_INPUT_DB15))<<(4) | (sampled_data_V1&(1<<C_INPUT_DB14))<<(1)  | (sampled_data_V1&(1<<C_INPUT_DB13))<<(5) | (sampled_data_V1&(1<<C_INPUT_DB12))<<(2)  | (sampled_data_V1&(1<<C_INPUT_DB11))<<(2) | (sampled_data_V1&(1<<C_INPUT_DB10))>>(2)  | (sampled_data_V1&(1<<C_INPUT_DB9))<<(4)  | ((sampled_data_V1&1)<<8) /*8 MSBs*/        |        /*8 LSBs*/(sampled_data_V1&(1<<C_INPUT_DB7)) | (sampled_data_V1&(1<<C_INPUT_DB6))<<(2) | (sampled_data_V1&(1<<C_INPUT_DB5))<<(2) |	(sampled_data_V1&(1<<C_INPUT_DB4))<<(3)	| (sampled_data_V1&(1<<C_INPUT_DB15))>>(8) | (sampled_data_V1&(1<<C_INPUT_DB15))>>(9) | (sampled_data_V1&(1<<C_INPUT_DB15))>>(10) | (sampled_data_V1&(1<<C_INPUT_DB15))>>(11)       );
			buffer[memADDR+1] = sampled_data_V1;  //valor da input V1+ do ADCs


			//V2
			//PARALLEL ->buffer[memADDR+2] = ((sampled_data_V2&(1<<C_INPUT_DB15))<<(4) | (sampled_data_V2&(1<<C_INPUT_DB14))<<(1)  | (sampled_data_V2&(1<<C_INPUT_DB13))<<(5) | (sampled_data_V2&(1<<C_INPUT_DB12))<<(2)  | (sampled_data_V2&(1<<C_INPUT_DB11))<<(2) | (sampled_data_V2&(1<<C_INPUT_DB10))>>(2)  | (sampled_data_V2&(1<<C_INPUT_DB9))<<(4)  | ((sampled_data_V2&1)<<8) /*8 MSBs*/        |        /*8 LSBs*/(sampled_data_V2&(1<<C_INPUT_DB7)) | (sampled_data_V2&(1<<C_INPUT_DB6))<<(2) | (sampled_data_V2&(1<<C_INPUT_DB5))<<(2) |	(sampled_data_V2&(1<<C_INPUT_DB4))<<(3)	| (sampled_data_V2&(1<<C_INPUT_DB15))>>(8) | (sampled_data_V2&(1<<C_INPUT_DB15))>>(9) | (sampled_data_V2&(1<<C_INPUT_DB15))>>(10) | (sampled_data_V2&(1<<C_INPUT_DB15))>>(11)     );
//DEBUG
			//buffer[memADDR+2] = reference_vector_DEBUG_TESTE1;

			buffer[memADDR+2] = sampled_data_V2; //valor da input V2+ do ADC

			//V3
			//PARALLEL ->buffer[memADDR+3] = ((sampled_data_V3&(1<<C_INPUT_DB15))<<(4) | (sampled_data_V3&(1<<C_INPUT_DB14))<<(1)  | (sampled_data_V3&(1<<C_INPUT_DB13))<<(5) | (sampled_data_V3&(1<<C_INPUT_DB12))<<(2)  | (sampled_data_V3&(1<<C_INPUT_DB11))<<(2) | (sampled_data_V3&(1<<C_INPUT_DB10))>>(2)  | (sampled_data_V3&(1<<C_INPUT_DB9))<<(4)  | ((sampled_data_V3&1)<<8) /*8 MSBs*/        |        /*8 LSBs*/(sampled_data_V3&(1<<C_INPUT_DB7)) | (sampled_data_V3&(1<<C_INPUT_DB6))<<(2) | (sampled_data_V3&(1<<C_INPUT_DB5))<<(2) |	(sampled_data_V3&(1<<C_INPUT_DB4))<<(3)	| (sampled_data_V3&(1<<C_INPUT_DB15))>>(8) | (sampled_data_V3&(1<<C_INPUT_DB15))>>(9) | (sampled_data_V3&(1<<C_INPUT_DB15))>>(10) | (sampled_data_V3&(1<<C_INPUT_DB15))>>(11)     );
			buffer[memADDR+3] = sampled_data_V3;
///DEBUG

			//TESTAR ISTO EM CASOS ONDE O sYNC OCORRE DURANTE O CONVSTA...
			//buffer[memADDR+3] = reference_vector_DEBUG;  //valor da input V3+ do ADC


			//dont use (sampled_data_V3&(1<<0))<<(1)


			shared_ram[PRU1PROG_WROTE_DATA] = write_pointer_count;//sample_counter; //none data wrotten //automatically reset when 2^16 get reached
			if (COMPARE_VAL(memADDR,(C_BUFFER_LENGTH-4)))
			{
				memADDR = 0;
			}
			else
			{
				if (freeze_addrs_flag == 0)
				{
					ADD_VAL(memADDR,4,memADDR); //each point has 4 words with 16 bits length
				}
			}

			//ADD_ONE(sample_counter,sample_counter);
			sample_counter++;
#ifdef C_NORMAL_OPPERATION
	    		//Increment and set 1PPS Led
				if (sample_counter < samples_per_second/2)
				{
					SET_BITV2(__R30,C_OUTPUT_LED3_1PPS);
				}
				else
				{
					CLR_BITV2(__R30,C_OUTPUT_LED3_1PPS);
				}
#endif
		} //if  COMPARE_VAL((__R31&(1<<C_BUSY_INPUT)) , (1<<C_BUSY_INPUT))

	   }
	   else
	   { //if(read_adc_or_emulate_operation_mode == 0x0001){ 	//Reads from emulated samples...
			emulated_mode_sample_generated = 0;
			while(PWMSS0.EPWM_TBCNT <= 1000)
			{

				if(emulated_mode_sample_generated == 0)
				{
					if(write_pointer_count == 0xFFFF)
					{
						write_pointer_count = 1;
					}
					else
					{
						write_pointer_count++; //increment the write counter to identify a new data
					}

					//Send data to the shared memory buffer.
					if ((PWMSS0.EPWM_TBSTS & 0x0002) == 0x0002)
					{ //Just to take shure that until the data send, there is no sync, without the sync reset
						//Syncs PRU0
						sinc_1pps_counter++;
						shared_ram[PRU1_TO_PRU0_SYNC_CNTR] = sinc_1pps_counter;	//Syncs a PRU0

						sample_counter = 1;
						PWMSS0.EPWM_TBSTS = 0x0002;
					}
					buffer[memADDR] = sample_counter; //1 until FS
					//V1
					buffer[memADDR+1] = AD_read_val_faseA[i];  //valor da input V1+ do ADCs
					//V2
					//DEBUG
					//buffer[memADDR+2] = reference_vector_DEBUG_TESTE1;
					buffer[memADDR+2] = AD_read_val_faseB[i]; //valor da input V2+ do ADC
					//V3
					buffer[memADDR+3] = AD_read_val_faseC[i];
					shared_ram[PRU1PROG_WROTE_DATA] = write_pointer_count;//sample_counter; //none data wrotten //automatically reset when 2^16 get reached

					if (COMPARE_VAL(memADDR,(C_BUFFER_LENGTH-4)))
					{
						memADDR = 0;
					}
					else
					{
						ADD_VAL(memADDR,4,memADDR); //each point has 4 words with 16 bits length
					}
					i++;
					if (i >= C_SAMPLES_PER_CYCLE)
					{
						i=0;
					}
					//if (sample_counter == C_SAMPLED_FREQUENCY){
					//	sample_counter = 0;
					//}
					sample_counter++;
					emulated_mode_sample_generated = 1;		//Flag variable that stops the data write to the memory until the next conversion request emulation.
#ifdef C_NORMAL_OPPERATION
					//Increment and set 1PPS Led
					if (sample_counter < samples_per_second/2)
					{
						SET_BITV2(__R30,C_OUTPUT_LED3_1PPS);
					}
					else
					{
						CLR_BITV2(__R30,C_OUTPUT_LED3_1PPS);
					}
#endif
				}

				/*if ((PWMSS0.EPWM_TBSTS & 0x0002) == 0x0002){
					sample_counter = 1;
					PWMSS0.EPWM_TBSTS = 0x0002;
					TGL_BITV2(__R30,C_OUTPUT_LED2_PRU1); 	//muda estado LED3
				};*/

		   }






	   } //end of if(read_adc_or_emulate_operation_mode == 0x0001)





   } //while (1) main loop
}//main




void REQUEST_CONVERSION()
{ //bitbang
		register uint8_t j;
		//DEFAULT VALUE (FLOATING PINS - ON THIS ENVIRONMENT) DB4 TO DB15 = 011001100010 VERIFICAR ISTO SEM A SHIELD
		sampled_data_V1 = 0;
		sampled_data_V2 = 0;
		sampled_data_V3 = 0;
		while( __R31&C_BUSY_INPUT )
		{
		} //wait until busy goes low dow.
		////////////////////////////////////////////////////	ENABLING DATA READ PINS - READ V1(18:2) ////////////////////////////////////////////////////
		CLR_BITV2(__R30,C_CS_OUTPUT); //enabling data transfer

		//Reading V1
		for (j = 0; j <= 15; j++)
		{ //reading 16 usefull bits
			CLR_BITV2(__R30,C_RDSCL_OUTPUT); //enabling read
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
			if ((__R31 & C_DBoutA_SPI_RD))
			{
				sampled_data_V1 |= BIT0;
			}
			if(j<15)
			{
				sampled_data_V1 = (sampled_data_V1<<1);
			}
			SET_BITV2(__R30,C_RDSCL_OUTPUT);
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
		}

		if (adc_18bit_datalength != 0) //USED for 18-bits ADCs
		{
			for (j = 0; j <= 1; j++)
			{ //reading 2 not used bits
				CLR_BITV2(__R30,C_RDSCL_OUTPUT); //enabling read
				__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
				SET_BITV2(__R30,C_RDSCL_OUTPUT);
				__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
			}
		}

		//Reading V2
		for (j = 0; j<=15; j++)
		{ //reading 16 usefull bits
			CLR_BITV2(__R30,C_RDSCL_OUTPUT); //enabling read
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
			if ((__R31 & C_DBoutA_SPI_RD))
			{
				sampled_data_V2 |= BIT0;
			}
			if(j<15)
			{
				sampled_data_V2 = (sampled_data_V2<<1);
			}
			SET_BITV2(__R30,C_RDSCL_OUTPUT);			SET_BITV2(__R30,C_RDSCL_OUTPUT);
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
		}

		if (adc_18bit_datalength != 0) //USED for 18-bits ADCs
		{
			for (j = 0; j <= 1; j++) //USED for 18-bits ADCs
			{ //reading 2 not used bits
				CLR_BITV2(__R30,C_RDSCL_OUTPUT); //enabling read
				__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
				SET_BITV2(__R30,C_RDSCL_OUTPUT);
				__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
			}
		}

		//Reading V3
		for (j = 0; j<=15; j++)
		{ //reading 16 usefull bits
			CLR_BITV2(__R30,C_RDSCL_OUTPUT); //enabling read
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
			if ((__R31 & C_DBoutA_SPI_RD))
			{ //se bit setado, faz o set do bit no vetor
				sampled_data_V3 |= BIT0;
			}
			if(j<15)
			{
				sampled_data_V3 = (sampled_data_V3<<1);
			}
			SET_BITV2(__R30,C_RDSCL_OUTPUT);
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
		}
/*		for (j = 0; j<=1; j++)  //USED for 18-bits ADCs
		{ //reading 2 not used bits
			CLR_BITV2(__R30,C_RDSCL_OUTPUT); //enabling read
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
			SET_BITV2(__R30,C_RDSCL_OUTPUT);
			__delay_cycles(C_DELAY_RDSCL_LPULSE_BEFORE_READ); //5 cycles us the minimun time to read a valid data on __R31 pin, T_14 = 24ns to 3v3 Vdrive
		}
*/
		SET_BITV2(__R30,C_CS_OUTPUT); 	//idle state to chip select
}


