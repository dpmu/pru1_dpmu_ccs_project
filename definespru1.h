 /*
 * definespru1.h
 *
 *  Created on: 6 de jun de 2020
 *      Author: Maique Garcia
 */

#ifndef DEFINESPRU1_H_
#define DEFINESPRU1_H_

#define HIGH 1
#define LOW 0
#define LOW_TO_HIGH 1
#define HIGH_TO_LOW -1
#define NO_TRANSICTION 0

typedef enum
{
	FSM_INIT,
	FSM_WAIT_REQ,
	FSM_CONVERSION_REQ,
	FSM_WAIT_BUSY,
	FSM_CONVERSION_END,
	FSM_COMPENSATE_PWM,
} t_fsm_sampling_state;

typedef enum
{
	BUSY_SIGNAL,
	CONVREQ_SIGNAL,
} t_transictions_signals;

/************************************************************
* STANDARD BITS
************************************************************/
//16LSB
#define BIT0                   (0x00000001)
#define BIT1                   (0x00000002)
#define BIT2                   (0x00000004)
#define BIT3                   (0x00000008)
#define BIT4                   (0x00000010)
#define BIT5                   (0x00000020)
#define BIT6                   (0x00000040)
#define BIT7                   (0x00000080)
#define BIT8                   (0x00000100)
#define BIT9                   (0x00000200)
#define BIT10                  (0x00000400)
#define BIT11                  (0x00000800)
#define BIT12                  (0x00001000)
#define BIT13                  (0x00002000)
#define BIT14                  (0x00004000)
#define BIT15                  (0x00008000)
//16MSB
#define BIT16                  (0x00010000)
#define BIT17                  (0x00020000)
#define BIT18                  (0x00040000)
#define BIT19                  (0x00080000)
#define BIT20                  (0x00100000)
#define BIT21                  (0x00200000)
#define BIT22                  (0x00400000)
#define BIT23                  (0x00800000)
#define BIT24                  (0x01000000)
#define BIT25                  (0x02000000)
#define BIT26                  (0x04000000)
#define BIT27                  (0x08000000)
#define BIT28                  (0x10000000)
#define BIT29                  (0x20000000)
#define BIT30                  (0x40000000)
#define BIT31                  (0x80000000)


#endif /* DEFINESPRU1_H_ */
